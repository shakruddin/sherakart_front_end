import Vue from 'vue'
import './plugins/vuetify'
import App from './App.vue'
import router from './router'
import NProgress from 'nprogress'
import './registerServiceWorker'
import VueJWT from 'vuejs-jwt'
import VuetifyToast from 'vuetify-toast-snackbar'
import '../node_modules/nprogress/nprogress.css'

router.beforeResolve((to, from, next) => {
  if (to.name) {
    NProgress.start()
  }
  next()
})

router.afterEach(() => {
  NProgress.done()
})


Vue.use(VuetifyToast, {
	x: 'right', // default
	y: 'bottom', // default
	color: 'info', // default
	icon: 'info',
	timeout: 3000, // default
	dismissable: true, // default
	autoHeight: false, // default
	multiLine: false, // default
	vertical: false, // default
	shorts: {
		custom: {
			color: 'purple'
		}
	},
	property: '$toast' // default
})

Vue.use(VueJWT)

// false producntion tip
Vue.config.productionTip = false

// private area
router.beforeEach((to, from, next) => {
  if (to.meta.requiresAuth) {
    const authUser = (window.localStorage.getItem('token'))
    if (authUser) {
      const verifyUserData = Vue.$jwt.decode(authUser, 'vZiIpmTzqXHp8PpYXTwqc9SRQ1UAyAfC')
      if(verifyUserData.isValidUser){
        next()
      }else{
        next({name: 'home'})
      }
    }else{
      next({name: 'home'})
    }
  }
  next()
})

new Vue({
  router,
  render: h => h(App)
}).$mount('#app')
