import Vue from 'vue'
import Vuetify, { VSnackbar, VBtn, VIcon } from 'vuetify/lib'
import VuetifyToast from 'vuetify-toast-snackbar'
import 'vuetify/src/stylus/app.styl'
import '@fortawesome/fontawesome-free/css/all.css'

Vue.use(Vuetify, {
  // iconfont: 'fa',
  components: {
    VSnackbar,
    VBtn,
    VIcon
  },
  theme: {
    primary: '#FF9800',
  }
})


Vue.use(VuetifyToast)