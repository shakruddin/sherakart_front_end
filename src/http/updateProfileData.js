import axios from 'axios';

const editUserDataUri = 'api/edit/personal/info'
const updatePassword = 'api/change/password'

class PostUserData {

    // update user data
    static insertPost(data, _id){
      return axios.post(`${editUserDataUri}/${_id}`, {
        //   User Details
          firstName:data.firstName,
          lastName:data.lastName,
          gender: data.gender
      })
      .then(function(res){
          return(res)
      }).catch((err) =>{
          return(err)
      })
    }

    // update user password
    static updatePassword(data, _id, username){
      return axios.put(`${updatePassword}/${_id}/${username}`, {
        //   User passowrd old and new
        currentPassword: data.currentPassword,
        password: data.password
      })
      .then(function(res){
          return(res)
      }).catch((err) =>{
          return(err)
      })
    }
}

export default PostUserData;