import axios from 'axios';

const getUrl = 'api/compo/cat'

class PostComponent {
    static getcomponent() {
        return new Promise(async (resolve, reject) => {
           try {
               const res = await axios.get(getUrl)
               return resolve(res)
           }catch(err){
               return reject(err);
           }
        })
      }
}

export default PostComponent;