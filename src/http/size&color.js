import axios from 'axios';

// sizes url
const url = 'api/get-size-data'
const postUrlSize = 'api/post-size-data'
const deleteSize = 'api/delete-size-data'
const updateUrlSize = 'api/update-size-data'

// color url
const colorurl = 'api/get-color-data'
const postUrlColor = 'api/post-color-data'
const deleteColor = 'api/delete-color-data'
const updateUrlcolor = 'api/update-color-data'

class postSizeColor {
    // getting size
    static getSize(){
        return new Promise(async (resolve, reject) => {
            try {
                const res = await axios.get(url)
                return resolve(res)
            }catch(err){
                return reject(err);
            }
        })
    }
    // post size item
    static postSize(data){
        return axios.post( postUrlSize, {
            wearComponent: data.wearComponent,
            size: data.size
        })
        .then(function(res){
            return(res)
        })
        .catch(function(err){
            throw(err)
        });
    }
    // end

    // delete size item
    static deleteSize(_id) {
        return new Promise(async (resolve, reject) => {
           try {
               const res = await axios.delete(`${deleteSize}?_id=${_id}`)
               return resolve(res)
           }catch(err){
               return reject(err);
           }
        })
    }
    // end

    // update size
    static updateSize(_id, data){
        return axios.put(`${updateUrlSize}?_id=${_id}`, {
            wearComponent: data.wearComponent,
            size:data.size,
        })
        .then(function(res){
            return(res)
        }).catch((err) =>{
            return(err)
        })
      }
    // end

    // getting Color
    static getColor(){
        return new Promise(async (resolve, reject) => {
            try {
                const res = await axios.get(colorurl)
                return resolve(res)
            }catch(err){
                return reject(err);
            }
        })
    }
    // post size item
    static postColor(data){
        return axios.post( postUrlColor, {
            color: data.color
        })
        .then(function(res){
            return(res)
        })
        .catch(function(err){
            throw(err)
        });
    }
    // end

    // delete size item
    static deleteColor(_id) {
        return new Promise(async (resolve, reject) => {
           try {
               const res = await axios.delete(`${deleteColor}?_id=${_id}`)
               return resolve(res)
           }catch(err){
               return reject(err);
           }
        })
    }
    // end

    // update size
    static updateColor(_id, data){
        return axios.put(`${updateUrlcolor}?_id=${_id}`, {
            color:data.color,
        })
        .then(function(res){
            return(res)
        }).catch((err) =>{
            return(err)
        })
      }
    // end
}

export default postSizeColor;