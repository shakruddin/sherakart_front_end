import axios from 'axios';

const url = 'api/login';
const createUser = 'api/create/users'
const verifyOTP = 'api/verification/otp/';
const createAdmin = 'api/create/admin';
const deleteuser = 'api/get-delete-user'
const getOtp = 'api/get/otp/change/password'
const changePassword = 'api/change/password'
const veriryotp = 'api/verify/otp'

class PostState {
    // admin can delete users data
    static deleteUser(_id) {
      return new Promise(async (resolve, reject) => {
         try {
             const res = await axios.delete(`${deleteuser}?_id=${_id}`)
             return resolve(res)
         }catch(err){
             return reject(err);
         }
      })
    }
    
    // Posts
    static LoginUser(data){
      return axios.post(url, {
        username : data.username,
        password: data.password,    
      })
      .then(function(res){
          return(res)
      }).catch((err) =>{
          throw err
      })
    }

    // forgot password
    static getOtpUpdatePass(username) {
      return new Promise(async (resolve, reject) => {
         try {
             const res = await axios.get(`${getOtp}/${username}`)
             return resolve(res)
         }catch(err){
             return reject(err);
         }
      })
    }
    // verify otp
    static verifyPhoneNumber(data, username){
      return axios.post(`${veriryotp}/${username}`, {
        otp : data.otp,
      })
      .then(function(res){
          return(res)
      }).catch((err) =>{
          throw err
      })
    }

    // update password
    static UpdatePassword(data, username){
      return axios.put(`${changePassword}/${username}`, {
        password: data.password,    
        otp: data.otp 
      })
      .then(function(res){
          return(res)
      }).catch((err) =>{
          throw err
      })
    }

    // creating user
    static insertPost(data){
      return axios.post(createUser, {
        //   User Details
          firstName:data.firstName,
          lastName:data.lastName,
          phoneNumber: data.phoneNumber,
          email: data.email,
          password: data.password,
          gender: data.gender
      })
      .then(function(res){
          return(res)
      }).catch((err) =>{
          return(err)
      })
    }

    // creating admin
    static createAdmin(data){
      return axios.post(createAdmin, {
        //   admin Details
          firstName:data.firstName,
          lastName:data.lastName,
          phoneNumber: data.phoneNumber,
          email: data.email,
          password: data.password,
          gender: data.gender
      })
      .then(function(res){
          return(res)
      }).catch((err) =>{
          return(err)
      })
    }

    static verifyOtp(_id, otp){
      return axios.get(`${verifyOTP}${_id}/${otp}`);
    }
}

export default PostState;