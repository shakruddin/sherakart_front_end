import axios from 'axios';

const url = 'api/products';
const specficData = 'api/product';
const favoriteUrl = 'api/add/to/favorite/list'
const refToken = 'api/refresh/token'
const getFavoriteList = 'api/favorite/list'
const moveToBeg = 'api/move/to/beg'
const getbagitem = 'api/bag/list'
const removeItem = 'api/remove/bag/item'
const saveForLaterUrl = 'api/save/for/later/item'
const increaseQty = 'api/update/quantity'
const orderUrl = 'api/add/order/list'
const userOrders = 'api/get/user/orders'
const search = 'api/search'

 
class PostPoduct {
 
    // search products
    static search(product, count){
        return new Promise(async (resolve, reject) => {
            try {
                const res = await axios.get(`${search}?product=${product}&count=${count}`)
                return resolve(res)
            }catch(err){
                return reject(err);
            }
        })
    }
    // end

    // getting user orders
    static userOrders(_id){
        return new Promise(async (resolve, reject) => {
            try {
                const res = await axios.get(`${userOrders}?_id=${_id}`)
                return resolve(res)
            }catch(err){
                return reject(err);
            }
        })
    }
    // end

    // move to bag from favorite list
    static moveToBag(data, _id, productId){
        return axios.post( `${moveToBeg}/list?_id=${_id}&productId=${productId}`, 
            data
        )
        .then(function(res){
            return(res)
        })
        .catch(function(err){
            throw(err)
        });
    }
    // end

    // move to orderList from favorite list
    static moveToOrderList(data, _id){
        return axios.post( `${orderUrl}?_id=${_id}`, {
            paymentType: data.paymentType,
            Address: data.Address,
            productDetails: data.productDetails,
            paymentData: data.paymentData
        })
        .then(function(res){
            return(res)
        })
        .catch(function(err){
            throw(err)
        });
    }
    // end
    
    // increase qty for product
    static increaseQtyFunc(_id, productId, data){
        return axios.put(`${increaseQty}?_id=${_id}&productId=${productId}`, {
            Qty:data.Qty,
            footSize:data.footSize,
        })
        .then(function(res){
            return(res)
        }).catch((err) =>{
            return(err)
        })
      }
    // end

    // move to bag from favorite list
    static moveToBagSpecfic(data, _id, productId){
        return axios.post( `${moveToBeg}?_id=${_id}&productId=${productId}`, 
            data
        )
        .then(function(res){
            return(res)
        })
        .catch(function(err){
            throw(err)
        });
    }
    // end

    // removeing bag item
    static removeBagItem(_id, productId){
        return new Promise(async (resolve, reject) => {
            try {
                const res = await axios.get(`${removeItem}?_id=${_id}&productId=${productId}`)
                return resolve(res)
            }catch(err){
                return reject(err);
            }
        })
    }
    // end

    // saving for later
    static saveForLater(data, _id, productId){
        return axios.post( `${saveForLaterUrl}?_id=${_id}&productId=${productId}`, 
            data
        )
        .then(function(res){
            return(res)
        })
        .catch(function(err){
            throw(err)
        });
    }
    // end

    // getting begList token
    static getBagList(_id){
        return new Promise(async (resolve, reject) => {
            try {
                const res = await axios.get(`${getbagitem}?_id=${_id}`)
                return resolve(res)
            }catch(err){
                return reject(err);
            }
        })
    }
    
    static getProducts(productName, count) {
        return new Promise(async (resolve, reject) => {
           try {
               const res = await axios.get(`${url}/${productName}?count=${count}`)
               return resolve(res)
           }catch(err){
               return reject(err);
           }
        })
    }

    static getProductById(_id) {
        return new Promise(async (resolve, reject) => {
           try {
               const res = await axios.get(`${specficData}?_id=${_id}`)
               const data = res.data.data
               return resolve(data)
           }catch(err){
               return reject(err);
           }
        })
    }

    // getting refresh token
    static getRefreshToken(_id){
        return new Promise(async (resolve, reject) => {
            try {
                const res = await axios.get(`${refToken}/${_id}`)
                return resolve(res)
            }catch(err){
                return reject(err);
            }
        })
    }

    // getting refresh token
    static getFavoriteList(_id){
        return new Promise(async (resolve, reject) => {
            try {
                const res = await axios.get(`${getFavoriteList}?_id=${_id}`)
                return resolve(res)
            }catch(err){
                return reject(err);
            }
        })
    }

     // store favorite item in list
     static favoriteItem(data, _id, userId){
        return axios.post( `${favoriteUrl}?_id=${_id}&productId=${userId}`, 
            data
        )
        .then(function(res){
            return(res)
        })
        .catch(function(err){
            throw(err)
        });
    }
}

export default PostPoduct;