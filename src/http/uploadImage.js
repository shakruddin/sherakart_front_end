import axios from 'axios';

const url = 'api/cover-image-upload';
const getUrl = 'api/getting-slider-data'
const deleteImageUrl = 'api/delete-image/aws'
const upload = 'api/upload-product'
const getProduct = 'api/products'
const prductLength = 'api/get-product-count'
const orders = 'api/get-product-orders'
const updateOrder = 'api/update-product-status'
const updateCompo = 'api/update-component-data'
const filterByG = 'api/filter-by-gender'
const ordreComp = 'api/get-all-complete-orders'
const updateProData = 'api/update-product/aws'
const deleteProductData = 'api/delete-product-data'

class PostState {
     // Get Posts
     static getPosts() {
        return new Promise(async (resolve, reject) => {
           try {
               const res = await axios.get(getUrl)
               const data = res.data
               return resolve(data)
           }catch(err){
               return reject(err);
           }
        })
    }

    // Get product count
    static productLength() {
        return new Promise(async (resolve, reject) => {
           try {
               const res = await axios.get(prductLength)
               return resolve(res)
           }catch(err){
               return reject(err);
           }
        })
    }
    // update status
    static updateOrderData(data, _id){
        return axios.put(`${updateOrder}?_id=${_id}`, {
          //   User Details
          status: data.status
        })
        .then(function(res){
            return(res)
        }).catch((err) =>{
            return(err)
        })
      }
       
      // Get orders
    static ordersCompleted() {
        return new Promise(async (resolve, reject) => {
           try {
               const res = await axios.get(ordreComp)
               return resolve(res)
           }catch(err){
               return reject(err);
           }
        })
    }

    //filter product by gender
    static genderByProduct(gender, typeOfWear, productCat, count) {
        return new Promise(async (resolve, reject) => {
           try {
               const res = await axios.get(`${filterByG}?gender=${gender}&typeOfWear=${typeOfWear}&productCat=${productCat}&count=${count}`)
               return resolve(res)
           }catch(err){
               return reject(err);
           }
        })
    }

    // Get orders
    static orders() {
        return new Promise(async (resolve, reject) => {
           try {
               const res = await axios.get(orders)
               return resolve(res)
           }catch(err){
               return reject(err);
           }
        })
    }
    
    static getProducts(count) {
        return new Promise(async (resolve, reject) => {
           try {
               const res = await axios.get(`${getProduct}?count=${count}`)
               const data = res.data
               return resolve(data)
           }catch(err){
               return reject(err);
           }
        })
    }

    // Posts
    static imageUpload(formData){
        return axios.post( url, 
            formData
        )
        .then(function(res){
            return(res)
        })
        .catch(function(err){
            throw(err)
        });
    }

    // updateing component data
    static updateComponentCatData(formData, _id){
        return axios.post( `${updateCompo}?_id=${_id}`, 
            formData
        )
        .then(function(res){
            return(res)
        })
        .catch(function(err){
            throw(err)
        });
    }

    static upload(formData){
        return axios.post( `${upload}/image`, 
            formData
        )
        .then(function(res){
            return(res)
        })
        .catch(function(err){
            throw(err)
        });
    }

    static uploadProductData(data){
        return axios.post( `${upload}/data`, {
            typeOfWear: data.typeOfWear,
            productCat:data.productCat,
            productName: data.productName,
            productPrice: data.productPrice,
            productDiscount: data.productDiscount,
            productActualPrice: data.productActualPrice,
            productColor: data.productColor,
            productGender: data.productGender,
            productSizes: data.productSizes,
            productQuntity: data.productQuntity,
            productDescription: data.productDescription,
            productRelativeImages: data.productRelativeImages
        })
        .then(function(res){
            return(res)
        })
        .catch(function(err){
            throw(err)
        });
    }

    static updateProductData(data, _id){
        return axios.put( `${updateProData}?_id=${_id}`, {
            typeOfWear: data.typeOfWear,
            productCat:data.productCat,
            productName: data.productName,
            productPrice: data.productPrice,
            productDiscount: data.productDiscount,
            productActualPrice: data.productActualPrice,
            productColor: data.productColor,
            productGender: data.productGender,
            productSizes: data.productSizes,
            productQuntity: data.productQuntity,
            productDescription: data.productDescription,
            productRelativeImages: data.productRelativeImages
        })
        .then(function(res){
            return(res)
        })
        .catch(function(err){
            throw(err)
        });
    }

    static deleteImage(_id) {
        return new Promise(async (resolve, reject) => {
           try {
               const res = await axios.delete(`${deleteImageUrl}?_id=${_id}`)
               return resolve(res)
           }catch(err){
               return reject(err);
           }
        })
      }

      static deleteProductData(_id) {
        return new Promise(async (resolve, reject) => {
           try {
               const res = await axios.delete(`${deleteProductData}?_id=${_id}`)
               return resolve(res)
           }catch(err){
               return reject(err);
           }
        })
      }
}

export default PostState;
