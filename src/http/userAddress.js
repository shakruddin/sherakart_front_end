import axios from 'axios';

const url = '/api/add/address';
const getAddress = 'api/get/address';
const deleteAddressUrl = 'api/delete/address';
const updateAddressUrl = 'api/update/address';
const getOtpUrl = 'api/get/otp/to-change/phone'
const verifyUpdate = 'api/update/phone'

class PostPoduct {

    static getAddress(_id) {
        return new Promise(async (resolve, reject) => {
           try {
               const res = await axios.get(`${getAddress}?_id=${_id}`)
            //    const data = res.data
               return resolve(res)
           }catch(err){
               return reject(err);
           }
        })
    }

    // getting OTP for update phone number
    static getOTP(data, _id){
        return axios.post( `${getOtpUrl}/${_id}`,{
            username: data.username,
            password: data.password,
            newNumber: data.newNumber,
        })
        .then(function(res){
            return(res)
        })
        .catch(function(err){
            throw(err)
        });
    }

    // update phone number if otp match 
    static verifyPhoneNumber(data, _id){
        return axios.post( `${verifyUpdate}/${_id}`,{
            phoneNumber: data.phoneNumber,
            otp: data.otp
        })
        .then(function(res){
            return(res)
        })
        .catch(function(err){
            throw(err)
        });
    }
    
     // adding address item into mongodb database 
    static addAddress(data, _id){
        return axios.post( `${url}?_id=${_id}`,{
            RecieverName: data.RecieverName,
            PhoneNumber: data.PhoneNumber,
            AlternateNo: data.AlternateNo,
            PinCode: data.PinCode,
            Address: data.Address,
            Locality: data.Locality,
            Landmark: data.Landmark,
            City: data.City,
            State: data.State,
            Country: data.Country,
            ThisIsMy: data.ThisIsMy

        })
        .then(function(res){
            return(res)
        })
        .catch(function(err){
            throw(err)
        });
    }

    // adding address item into mongodb database 
    static updateAddress(data, _id, addressID){
        return axios.post( `${updateAddressUrl}?_id=${_id}&addressID=${addressID}`,{
            RecieverName: data.RecieverName,
            PhoneNumber: data.PhoneNumber,
            AlternateNo: data.AlternateNo,
            PinCode: data.PinCode,
            Address: data.Address,
            Locality: data.Locality,
            Landmark: data.Landmark,
            City: data.City,
            State: data.State,
            Country: data.Country,
            ThisIsMy: data.ThisIsMy

        })
        .then(function(res){
            return(res)
        })
        .catch(function(err){
            throw(err)
        });
    }

    //   delete address
    static deleteAddress(_id, addressID) {
        return new Promise(async (resolve, reject) => {
           try {
               const res = await axios.get(`${deleteAddressUrl}?_id=${_id}&addressID=${addressID}`)
               return resolve(res)
           }catch(err){
               return reject(err);
           }
        })
      }
}

export default PostPoduct;