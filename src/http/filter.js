import axios from 'axios';

const filter = 'api/filter'

class PostPoduct {
    // move to orderList from favorite list
    static productfilter(data){
        return axios.post( `${filter}`, {
            size: data.size,
            color: data.color,
            gender: data.gender
        })
        .then(function(res){
            return(res)
        })
        .catch(function(err){
            throw(err)
        });
    }
    // end
}

export default PostPoduct;