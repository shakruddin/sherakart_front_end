import axios from 'axios';

// sizes url
const url = 'api/subscribe'
const subscribers = 'api/get-all-suscribers'
const allUsers = 'api/get-all-users'
const deleteSub = 'api/delete-suscribers'
const serachUser = 'api/search-users'
const searchOrder = 'api/search-orders'
const unset = 'api/unset/order/list'

class postSizeColor {
    // clear order list user

    // search users
    static unsetOrder(_id){
        return new Promise(async (resolve, reject) => {
            try {
                const res = await axios.get(`${unset}?_id=${_id}`)
                return resolve(res)
            }catch(err){
                return reject(err);
            }
        })
    }
    // end

    // search users
    static searchOrder(count, RecieverName, PhoneNumber, Address){
        return new Promise(async (resolve, reject) => {
            try {
                const res = await axios.get(`${searchOrder}?count=${count}&RecieverName=${RecieverName}&PhoneNumber=${PhoneNumber}&Address=${Address}`)
                return resolve(res)
            }catch(err){
                return reject(err);
            }
        })
    }
    // end

    // geting all subscribers
    static subscribers(count){
        return new Promise(async (resolve, reject) => {
            try {
                const res = await axios.get(`${subscribers}?count=${count}`)
                return resolve(res)
            }catch(err){
                return reject(err);
            }
        })
    }
    // end

    // search users
    static searchUsers(count, firstName, email, phoneNumber){
        return new Promise(async (resolve, reject) => {
            try {
                const res = await axios.get(`${serachUser}?count=${count}&firstName=${firstName}&email=${email}&phoneNumber=${phoneNumber}`)
                return resolve(res)
            }catch(err){
                return reject(err);
            }
        })
    }
    // end

    // geting all users
    static users(){
        return new Promise(async (resolve, reject) => {
            try {
                const res = await axios.get(`${allUsers}`)
                return resolve(res)
            }catch(err){
                return reject(err);
            }
        })
    }
    // end

    // post size item
    static userSubscribe(data){
        return axios.post( url, {
            email: data.email
        })
        .then(function(res){
            return(res)
        })
        .catch(function(err){
            throw(err)
        });
    }
    // end

    //   delete address
    static deleteSubs(_id) {
        return new Promise(async (resolve, reject) => {
           try {
               const res = await axios.delete(`${deleteSub}?id=${_id}`)
               return resolve(res)
           }catch(err){
               return reject(err);
           }
        })
      }
}

export default postSizeColor;