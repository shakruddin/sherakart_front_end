import axios from 'axios';

const getUrl = 'api/product-cat'
const createProductCat = 'api/add/procduct-category'
const deleteImageUrl = 'api/product-cat/delete'
const updateProductCat = 'api/update/product-cat'

class PostProductCat {
    static getProductCat() {
        return new Promise(async (resolve, reject) => {
           try {
               const res = await axios.get(getUrl)
               return resolve(res)
           }catch(err){
               return reject(err);
           }
        })
      }

      // creating product Category
    static insertPost(data){
        return axios.post(createProductCat, {
          //   User Details
          productType: data.productType,
          productCateName: data.productCateName
        })
        .then(function(res){
            return(res)
        }).catch((err) =>{
            return(err)
        })
      } 

      // creating product Category
    static updateProductCat(_id, data){
        return axios.put(`${updateProductCat}?_id=${_id}`, {
          //   User Details
          productType: data.productType,
          productCateName: data.productCateName
        })
        .then(function(res){
            return(res)
        }).catch((err) =>{
            return(err)
        })
      }

    //   deleting procduct category
    static deleteProductCat(_id) {
        return new Promise(async (resolve, reject) => {
           try {
               const res = await axios.get(`${deleteImageUrl}?_id=${_id}`)
               return resolve(res)
           }catch(err){
               return reject(err);
           }
        })
      }
}

export default PostProductCat;