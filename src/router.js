import Vue from 'vue'
import Router from 'vue-router'

Vue.use(Router)

export default new Router({
  // mode: 'history',
  base: process.env.BASE_URL,
  routes: [
    {
      path: '/filter', 
      name: 'filter',
      component: () => import('./views/UserInterface/ProductFilter/FilterProscription.vue')
    },
    {
      path: '/privacypolicy', 
      name: 'privacypolicy',
      component: () => import('./views/UserInterface/privacy&terms/privacy')
    },
    {
      path: '/contect-us', 
      name: 'contect-us',
      component: () => import('./views/UserInterface/privacy&terms/contectUs.vue')
    },
    {
      path: '/terms', 
      name: 'terms',
      component: () => import('./views/UserInterface/privacy&terms/terms')
    },
    {
      path: '/',
      name: 'home',
      component: () => import('./views/UserInterface/Home/Index.vue')
    },
    {
      path: '/product-detail',
      name: 'product-detail',
      component: () => import('./views/UserInterface/ProductCategory/ProductDetail.vue')
    },
    {
      path: '/cart',
      name: 'cart',
      component: () => import('./views/UserInterface/CardPay/cart.vue')
    },
    {
      path: '/wishlist',
      name: 'wishlist',
      meta: {
        requiresAuth: true,
      },
      component: () => import('./views/UserInterface/WishList/WishList.vue')
    },
    {
      path: '/orderlist',
      name: 'orderlist',
      meta: {
        requiresAuth: true,
      },
      component: () => import('./views/UserInterface/OrderList/OrderList.vue')
    },
    {
      path: '/profile',
      name: 'profile',
      meta: {
        requiresAuth: true,
      },
      component: () => import('./views/UserInterface/Profile/Profile.vue')
    },
    {
      path: '/account',
      name: 'account',
      meta: {
        requiresAuth: true,
      },
      component: () => import('./views/UserInterface/Account/Account.vue')
    },
    {
      path: '/address',
      name: 'address',
      meta: {
        requiresAuth: true,
      },
      component: () => import('./views/UserInterface/addAddress/addAddress.vue')
    },
    {
      path: '/delivery',
      name: 'delivery',
      meta: {
        requiresAuth: true,
      },
      component: () => import('./views/UserInterface/deliveryProcess/deliveryProcess.vue')
    },



    // Admin Router
    {
      path: '/deshborad',
      name: 'deshborad',
      component: () => import('./views/AdminPanel/Deshborad/Deshborad.vue')
    },
    {
      path: '/add-sizes', 
      name: 'size',
      component: () => import('./views/AdminPanel/color&size/size.vue')
    },
    {
      path: '/component',
      name: 'component',
      component: () => import('./views/AdminPanel/AddProduct/AddProduct.vue')
    },
    {
      path: '/component-category',
      name: 'componentCategory',
      component: () => import('./views/AdminPanel/Category/ComponentCategories.vue')
    },
    {
      path: '/product-categories',
      name: 'productCategories',
      component: () => import('./views/AdminPanel/Category/ProductCategories.vue')
    },
    {
      path: '/product',
      name: 'product',
      component: () => import('./views/AdminPanel/AddProduct/product.vue')
    },
    {
      path: '/settings',
      name: 'setting',
      component: () => import('./views/AdminPanel/Settings/Setting.vue')
    },
    {
      path: '/track-subscribers',
      name: 'tracksubs',
      component: () => import('./views/AdminPanel/trackUsers/trackSubscribers.vue')
    },
    {
      path: '/complete-orders',
      name: 'complete',
      component: () => import('./components/DeshboradTable/completeOrder.vue')
    },
    {
      path: '/track-users',
      name: 'trackusers',
      component: () => import('./views/AdminPanel/trackUsers/trackUser.vue')
    }

  ],
  scrollBehavior() {
      return {x: 0, y: 0}
  }
})
