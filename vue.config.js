const manifestJSON = require('./public/manifest.json')
const path = require('path');

module.exports = {
    outputDir: path.resolve(__dirname, '../src/public'),
    devServer: {
        proxy: {
          '/api': {
            target: 'http://localhost:8000',
          }
        }
    },
    configureWebpack:{
        performance: {
          hints: false
        },
        optimization: {
          splitChunks: {
            minSize: 10000,
            maxSize: 250000,
          }
        }
    },
    pwa: {
        themeColor: manifestJSON.theme_color,
        workboxOptions: {
        runtimeCaching: [{
            urlPattern: new RegExp('https://fonts.(?:googleapis|gstatic).com/(.*)'),
            handler: 'cacheFirst',
            options: {
            cacheName: 'google-fonts',
            expiration: {
                maxEntries: 30000
            },
            cacheableResponse: {
                statuses: [0, 200]
            }
            }
        }]
        }
    }
}
